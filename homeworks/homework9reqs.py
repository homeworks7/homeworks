from random import randint


def get_random_number():
    """
    Returns:
        (int)
    """
    number = randint(1, 101)
    return number

def count():
    counter = int(input("За скільки разів вгадаєшь? "))
    return counter

def get_number_from_user():
    """
    Returns:
        (int)
    """
    while True:
        try:
            return int(input('Enter int: '))
        except:
            print('It\'s not int')


def check_numbers(to_guess, user_number):
    """

    Args:
        to_guess (int):
        user_number (int):

    Returns:
        (bool):
    """

    print(f'---> {to_guess}')
    if to_guess == user_number:
        return True
    else:
        return False


import time

def timer(func):
    def wrapper(*args, **kwargs):
        start = time.time()
        func(*args, **kwargs)
        end = time.time()
        print("Витрачено часу на виконання сек. ", end-start)
    return wrapper

@timer
def game():
    counting = count()
    number_to_guess = get_random_number()
    while True:
        if counting > 0:
            print("left", counting)
            counting = counting - 1
        elif counting == 0:
            print("GAME OVER")
            break
        user_number = get_number_from_user()
        if user_number in range(number_to_guess - 4, number_to_guess + 4):
            print("Гаряче")
        elif user_number in range(number_to_guess - 10, number_to_guess + 10):
            print("Холодно")
        if check_numbers(number_to_guess, user_number):
            print('You WIN!!!!')
            break

game()


