import requests
import datetime


url = "https://bank.gov.ua/NBU_Exchange/exchange?json"

result = requests.request("GET", url)
cl_result = result.json()
print(type(result))


def status_code(result):
    if 300 > result.status_code >= 200:
        print("passed")
        return True
    else:
        raise TypeError("Status code Failure")

def header_check(result):
    if result := result.headers.get('Content-Type'):
        if result == 'application/json; charset=utf-8':
            print("ok")
            pass
        else:
            raise TypeError
        return True


def get_currency():
    curr_list = ""
    for code in cl_result:
        other_cur = code["CurrencyCodeL"]
        uah_value = code["Amount"]
        curr_list += f"{other_cur} to UAH {uah_value} \n"
    return curr_list

print(get_currency())
if all([status_code(result), header_check(result)]):
    with open("NBU currency.txt", 'w') as f:
        f.write(f"Запит за {datetime.datetime.now()} \n")
        f.write(get_currency())
        f.close()