class Transport_vehicle:    #батьківський клас
    def __init__(self, max_speed, fuel_capcaity):
        self.max_speed = max_speed
        self.fuel_capacity = fuel_capcaity

class Car(Transport_vehicle):
    def __init__(self, brand, model, max_speed, fuel_capcaity):
        super().__init__(max_speed, fuel_capcaity)
        self.brand = brand
        self.model = model

class Airplane(Transport_vehicle):
    def __init__(self, max_passengers, airport, max_speed, fuel_capcaity ):
        super().__init__(max_speed, fuel_capcaity)
        self.max_passengers = max_passengers
        self.airport = airport


class Ship(Transport_vehicle):
    def __init__(self, ship_name, crew, location, max_speed, fuel_capcaity):
        super().__init__(max_speed, fuel_capcaity)
        self.crew = crew
        self.location = location
        self.ship_name = ship_name

salty_dog = Ship("Salty Dog", "12", "Antarctida", "80", "1300")
print(salty_dog.ship_name)
casual_car = Car("Tesla", "S", "220", "150")
print(casual_car.max_speed)
transatlantic_airplane = Airplane("250", "Juliani", "1200", "2000")
print(transatlantic_airplane.airport)
