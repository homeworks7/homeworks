class Vehicle():
    tank_level: float = 0
    odometer_value = 0

    def __init__(self, producer: str, model: str, year: str, tank_capacity: float, maxspeed: int,
                 fuel_consumption: float):
        self.producer = producer
        self.model = model
        self.year = year
        self.tank_capacity = tank_capacity
        self.maxspeed = maxspeed
        self.fuel_consumption = fuel_consumption

    def __repr__(self):
        res = f"Car model {self.model}, year of release {self.year}, km passed {self.odometer_value}, fuel left {self.tank_level}"
        return res

    def refueling(self):
        litres_fuel = self.tank_capacity - self.tank_level
        print(f"litres to fuel {litres_fuel}")
        self.tank_level = self.tank_level + litres_fuel
        print("Done")
        return self.tank_level

    def race(self, distance):

        fuel_cost = (distance * self.fuel_consumption) // 100
        if self.tank_level >= fuel_cost:
            km_passed = fuel_cost * 10
            self.tank_level = self.tank_level - fuel_cost

        else:

            km_passed = ((self.tank_level) // self.fuel_consumption) * 100
            self.tank_level = 0
        race_time = distance / ((self.maxspeed // 100) * 80)
        self.odometer_value += distance
        res = {"Km passed": km_passed, "fuel left in tank": self.tank_level, "time spend(hours)": race_time}
        return res


    def lend_fuel(self, other):

        if not isinstance(other, self.__class__):
            raise TypeError("Oh, its not a vehicle")
        if self.tank_level == 0 or other.tank_level == other.tank_capacity:
            print("Нічого страшного, якось розберуся")
        fuel_other = other.tank_level + self.tank_level
        self.tank_level = self.tank_level - self.tank_level
        other.tank_level = fuel_other
        res = f"Дякую, бро, виручив. Ти залив мені {fuel_other} літрів пального"
        return res

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            raise TypeError("Oh, its not a vehicle")
        if self.odometer_value == other.odometer_value:
            if self.year == other.year:
                return True






car1 = Vehicle("Tesla", "S", "2022", 350, 200, 10)
car2 = Vehicle("Tesla", "X", "2022", 350, 220, 10)
print(car1)
print(car1.refueling())
print(car1.race(2000))
print(car1)
print(car1.lend_fuel(car2))
print(car1)
print(car2)
if car1 == car2:
    print("yes")
car3 = Vehicle("Tesla", "S", "2022", 350, 200, 10)
car4 = Vehicle("Tesla", "X", "2022", 350, 220, 10)
if car3 == car4:
    print("yes")

"sorry"