import datetime
inss = "onceuponatime123!"

def is_valid_length(string: str, length: int = 10) -> bool:
    result = len(string) >= length
    return result


def check_spec_symbols(checked: int, numbers="1234567890!"):
    for number in checked:
        if number in numbers:
            return True

def check_symbols(checked: str, symbols='qwertyuiopasdfghjklzxcvbnm'):
    for symbol in checked:
        if symbol in symbols:
            return True

def wrap_validate(func):
    def wrapper(*args, **kwargs):
        if kwargs.get("password"):
            w = kwargs.get("password")
        else:
            raise AttributeError(f'no parameter "password" in arguments of function{func.__name__}')

        if all([is_valid_length(w), check_symbols(w), check_spec_symbols(w)]):
            print("Validation pass")
            result = {"result": str(func(*args, **kwargs)), "is_secure": True}
        else:
            result = {"result": str(func(*args, **kwargs)), "is_secure": False}
        return result
    return wrapper


@wrap_validate
def password1(password):
    result = f"password is {password} "
    if len(result) >= 100:
        cut_result = f"{result[:100]}..."
    elif len(result) < 100:
        cut_result = result
    return cut_result

@wrap_validate
def registartion(id=None, login=None, notes=None, password=None):
    date = datetime.date.today()
    result = f'User {login} created account on {date} with password "{password}". Additional information: {notes}'
    return result

print(type(registartion(22, "garrysmith", "dumb one",  password="Iggypop99")))

print(password1(password=inss))

if __name__ == "__main__":
    assert is_valid_length(inss) == True, "Assert 1 - Faliled"
    assert is_valid_length("one") == False, "Assert 2 - Faliled"
    assert is_valid_length("None") == False, "Assert 3 - Faliled"
    assert check_symbols(inss) == True, "Assert 4 - Failed"
    assert check_symbols("Gosko") == True, "Assert 5 - Failed"
    assert check_symbols("JohnCena") == True, "Assert 6 - Failed"
    assert check_spec_symbols("23456789!") == True, "Assert 7 - Failed"
    assert check_spec_symbols("!!!!!!!122331") == True, "Assert 8 - Failed"
    assert check_spec_symbols("!!!!!!!") == True, "Assert 9 - Failed"
    assert type(registartion(22, "garrysmith", "dumb one",  password="Iggypop99")) == dict, "Assert 10 - Failed"

