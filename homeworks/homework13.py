class Test:
    x = 5
    y = 65


intos = Test()


class Point:
    _x = None
    _y = None
    _f = None
    # @property  # getter
    # def x(self):
    #     return self._x
    #
    # @x.setter
    # def x(self, value):
    #     if not isinstance(value, int):
    #         raise TypeError
    #     self._x = value
    #
    # @property  # getter
    # def y(self):
    #     return self._y
    #
    # @y.setter
    # def y(self, value):
    #     if not isinstance(value, int):
    #         raise TypeError
    #     self._y = value

    def __init__(self, x_coord, y_coord, f_cord):
        self.x = x_coord
        self.y = y_coord
        self.f = f_cord


point1 = Point(8, 14, 16)
point2 = Point(4, 1, 1)
point3 = Point(5, 1, 2)
# point3 = Point(5, 4)
# Доопрацюйте класс Triangle з попередньої домашки наступним чином:
# обʼєкти классу Triangle можна порівнювати між собою (==, !=, >, >=, <, <=)
# print() обʼєкту классу Triangle показує координати його вершин
class Line:
    _begin = None
    _end = None

    def begin_setter(self, value):
        print("begin")
        if not isinstance(value, Point):
            raise TypeError
        self._begin = value

    def begin_getter(self):
        return self._begin

    begin = property(begin_getter, begin_setter)

    def end_setter(self, value):
        print("end_setter")
        if not isinstance(value, Point):
            raise TypeError
        self._end = value

    def end_getter(self):
        return self._end

    end = property(end_getter, end_setter)

    def __init__(self, begin_point, end_point):
        self.begin = begin_point
        self.end = end_point

    @property
    def length(self):
        print('in length_getter')
        return ((self.begin.x - self.end.x) ** 2 + (self.begin.y - self.end.y) ** 2) ** 0.5


class Triangle():
    _begin = None
    _end = None

    def begin_setter(self, value):
        if not isinstance(value, Point):
            raise TypeError
        self._begin = value

    def begin_getter(self):
        return self._begin

    begin = property(begin_getter, begin_setter)

    def end_setter(self, value):
        if not isinstance(value, Point):
            raise TypeError
        self._end = value

    def end_getter(self):
        return self._end

    end = property(end_getter, end_setter)

    def __init__(self, begin_point, end_point):
        self.begin = begin_point
        self.end = end_point

    @property
    def triangle(self):
        print("Calculating area of Triangle")
        A = (self.begin.x - self.end.x)
        B = (self.begin.y - self.end.y)
        C = (self.begin.f - self.end.f)
        S = (A + B + C) // 2
        area = (S * ((S - A) * (S - B) * (S - C))) ** 0.5
        return area


    def __repr__(self):
        res = f"(Point ({self.begin.x},{self.end.x}) Point({self.begin.y},{self.end.y}) Point({self.begin.f},{self.end.f})) "
        return res

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            raise TypeError
        print(self, other)
        return True


    def __lt__(self, other):
        if not isinstance(other, self.__class__):
            raise TypeError
        print(self, other)
        return True


    def __le__(self, other):
        if not isinstance(other, self.__class__):
            raise TypeError
        print(self, other)
        return True

    def __gt__(self, other):
        if not isinstance(other, self.__class__):
            raise TypeError
        print(self, other)
        return True


    def __ge__(self, other):
        if not isinstance(other, self.__class__):
            raise TypeError
        print(self, other)
        return True

    def __ne__(self, other):  # !=
        if not isinstance(other, self.__class__):
            raise TypeError
        print(self, other)
        return True

triangle_one = Triangle(point1, point2)
triangle_two = Triangle(point1, point2)
triangle_three = Triangle(point1, point3)
print(triangle_three)
if triangle_one != triangle_three:
    print("yes")

