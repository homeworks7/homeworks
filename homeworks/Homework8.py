import random

def go_choice():
    """
    Ця функція присвоює вибір гравця та компьютера
    :return:
    """
    choices = ["камінь", "ножиці", "папір"]
    computer = random.choice(choices)
    me = input("Камінь, ножиці чи бумага?").lower()
    while me not in choices:
        me = input("Камінь, ножиці чи бумага?").lower()
    # print(computer, me)
    return me,computer


def win_condition():
    """
    Ця функція порівнює вибір гравців та визначає переможця
    :return:
    """
    choices = go_choice()
    player = choices[0]
    computer = choices[1]
    if player == computer:
        print("Ви вибрали", player)
        print("Вибір компьютера", computer)
        print("Нічия")
    elif player == "камінь":
        if computer == "папір":
            print("Ви вибрали", player)
            print("Вибір компьютера", computer)
            print("Ви програли")
        if computer == "ножиці":
            print("Ви вибрали", player)
            print("Вибір компьютера", computer)
            print("Ви виграли")
    elif player == "папір":
        if computer == "ножиці":
            print("Ви вибрали", player)
            print("Вибір компьютера", computer)
            print("Ви програли")
        if computer == "камінь":
            print("Ви вибрали", player)
            print("Вибір компьютера", computer)
            print("Ви Виграли")
    elif player == "ножиці":
        if computer == "камінь":
            print("Ви вибрали", player)
            print("Вибір компьютера", computer)
            print("Ви програли")
        if computer == "папір":
            print("Ви вибрали", player)
            print("Вибір компьютера", computer)
            print("Ви Виграли")





