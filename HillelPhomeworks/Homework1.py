def parse(text) -> dict:
    splitted = text.lstrip('https://example.com/path/to/page?&=').rstrip("&").replace("&", " ")
    if len(splitted) == 0:
        return {}
    dictionary = dict(texts.split("=") for texts in splitted.split(" "))
    return dictionary


print(parse("https://example.com/path/to/page?name=ferret&color=purple"))
print(parse('https://example.com/path/to/page?name=ferret&color=purple&'))
print(parse('http://example.com/'))
print(parse('http://example.com/?'))
print(parse('http://example.com/?name=Dima'))


if __name__ == '__main__':
    assert parse('https://example.com/path/to/page?name=ferret&color=purple') == {'name': 'ferret', 'color': 'purple'}
    assert parse('https://example.com/path/to/page?name=ferret&color=purple&') == {'name': 'ferret', 'color': 'purple'}
    assert parse('http://example.com/') == {}
    assert parse('http://example.com/?') == {}
    assert parse('http://example.com/?name=Dima') == {'name': 'Dima'}

def parse_cookie(text) -> dict:

    convert = text
    print(convert)
    if len(convert) == 0:
        return {}
    dictionary = dict(texts.split("=", 1) for texts in convert.rstrip(";").split(";"))
    print(dictionary)
    return dictionary

if __name__ == '__main__':
    assert parse_cookie('name=Dima;') == {'name': 'Dima'}
    assert parse_cookie('') == {}
    assert parse_cookie('name=Dima;age=28;') == {'name': 'Dima', 'age': '28'}
    assert parse_cookie('name=Dima=User;age=28;') == {'name': 'Dima=User', 'age': '28'}
